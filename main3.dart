void main() {
  var app1 = App('wumDrop', 'Best Enterprise', 'Benjamin Claassen and Muneeb Samuels', '2015');
  print('App name: ${app1.name}');
  print('Category: ${app1.category}');
  print('Developer: ${app1.developer}');
  print('Year it won: ${app1.yearwon}');
  app1.Cap();
}

class App {
  String name = '';
  String category = '';
  String developer = '';
  String yearwon = '';

  App(String name, String category, String developer, String yearwon) {
    this.name = name;
    this.category = category;
    this.developer = developer;
    this.yearwon = yearwon;
    
  }
  
  //converting the app name to uppercase
  void Cap()
  {
    print("App name to uppercase: ${name.toUpperCase()}");
  }
}
